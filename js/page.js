var loc = window.location.pathname;
var segments = loc.split('/');
var breadcrumbs = '';
var currentPath = '/';
for (var i=0; i<segments.length; i++) {
	if (segments[i] !== '') {
		  currentPath += segments[i] + '/';
		  breadcrumbs += '<a href="' +  currentPath + '">' + window.decodeURIComponent(segments[i]) + '<\/a>';
	} else if (segments.length -1 !== i) {
		  currentPath += '';
		  breadcrumbs += '<a href="' + currentPath + '">Root<\/a>';
	}
}
document.getElementById('breadcrumbs').innerHTML = breadcrumbs;
function click_copy() {
	/* Get the text field */
	var copyText = document.getElementById("text_to_copy");

	/* Select the text field */
	copyText.select();

	/* Copy the text inside the text field */
	document.execCommand("copy");
	copyText.style.background = 'green'
	copyText.style.color = 'white'

	var msg = document.getElementById("copy_msg");
	msg.style.display = '';

}
